//
//  ViewController.swift
//  mundoanimal_22001387
//
//  Created by COTEMIG on 17/11/22.
//

import UIKit
import Alamofire
import Kingfisher


struct Animal: Decodable {
    let name: String
    let latin_name: String
    
}
class ViewController: UIViewController{
    var listaAnimal: [Animal]?
    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet weak var name: UINavigationItem!
    @IBOutlet weak var latim_name: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getAnimais()
    }
    
    @IBAction func recarregarImagem(_ sender: Any) {
        getAnimais()
       
    }

    func getAnimais(){
        AF.request("https://zoo-animal-api.herokuapp.com/animals/rand").responseDecodable(of: Animal.self){
            response in
            if let animal = response.value{
                self.imageView.kf.setImage(with: URL(string: animal.name))
                print(animal.name)
            }
        }
        }
    }



